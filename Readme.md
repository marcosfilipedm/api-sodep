Desafio Sodep.

Foi desenvolvido uma api em java com todos os endpoints necessários para realizar as tarefas propostas, tenho certeza absoluta que poderia estar mais completo, mas tive N contratempos pessoais. O que atrapalharam em questão do prazo.

No backend foi utilizado Spring Web, Spring Security e Spring Cloud para o contexto do AWS s3. A princípio o desenvolvimento foi com base em TDD, algo que deixei de lado pelo meu tempo perdido, mas tenho consciência que é a melhor metodologia para desenvolvimento seguro. Encriptação via Bcrypt, utilização de Mappers e DTOs e alguns tratamentos de exceções.

O token gerado é armazenado em Local Storage e é obrigatório em todas as requisições, ao menos que seja denotada como pública.

O front foi desenvolvido em angular 8, também poderia melhorar em alguns aspectos, mas cumpre o proposto.

Foi utilizado, por opção pessoal, o MySql no ambiente do Docker como banco de dados. Para criá-lo basta rodar o comando docker-compose up onde o arquivo docker-compose.yaml (raiz do projeto Spring) se encontra (é necessário o docker estar instalado). As credenciais estão no arquivo caso queiram acessar o banco de dados via algum SGBD.

Importante ressaltar que não tive nenhuma dificuldade em qualquer parte do desafio. A mudança de estratégia em algumas partes foi apenas para facilitar e agilizar a entrega do mesmo.

No mais a ideia do desafio é interessante, e a deixarei mais completa com o tempo, com as devidas alterações e não utilização da imagem da Sodep. O desafio sempre estará disponível para acesso no bitBucket, independente da avaliação do desafio.

Agradeço a oportunidade.
