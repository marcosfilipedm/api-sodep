package com.sodep.apisodep.exception;

public class InvalidEmailException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8540725310051777387L;

	private String msg;

	public InvalidEmailException(String msg) {
		super(msg);
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
