package com.sodep.apisodep.service;

import java.util.List;

import com.sodep.apisodep.model.dto.CommentDTO;
import com.sodep.apisodep.model.dto.CommentEntityDTO;

public interface CommentService {

	public boolean saveComment(CommentEntityDTO commentDTO);

	public List<CommentDTO> getCommentByPost(Integer postId);
}
