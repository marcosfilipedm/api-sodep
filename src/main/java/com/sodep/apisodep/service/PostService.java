package com.sodep.apisodep.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.sodep.apisodep.model.dto.PostEntityDTO;
import com.sodep.apisodep.model.dto.PostViewDTO;

public interface PostService {

	public PostEntityDTO savePost(String legend, String username, MultipartFile imageFile);
	
	public PostEntityDTO getPost(Integer idPost);
	
	public List<PostViewDTO> getPosts();
	
	public boolean alterPostStatus(Integer idPost);

	public List<PostEntityDTO> getMyPosts(String name);

	public void addAlbum(Integer idPost, Integer idAlbum);

	public List<String> albumPhotos(Integer album);
}
