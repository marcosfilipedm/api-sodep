package com.sodep.apisodep.service;

import java.util.List;

import com.sodep.apisodep.model.dto.AlbumEntityDTO;

public interface AlbumService {

	public boolean save(String userName, String albumName);

	public List<AlbumEntityDTO> getAlbum();

	public List<String> getPhotos(Integer idAlbum);
	
}
