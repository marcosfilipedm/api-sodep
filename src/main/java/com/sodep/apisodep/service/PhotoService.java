package com.sodep.apisodep.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.sodep.apisodep.model.dto.PhotoEntityDTO;

public interface PhotoService {

	public void savePhoto(MultipartFile imageFile, Integer idAlbum);

	public List<PhotoEntityDTO> getPhotos(Integer albumId);

}
