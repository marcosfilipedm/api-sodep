package com.sodep.apisodep.service;

import com.sodep.apisodep.exception.InvalidEmailException;
import com.sodep.apisodep.exception.InvalidPasswordException;

public interface LoginService {

	String login(String username, String password, String system) throws InvalidPasswordException, InvalidEmailException;
}
