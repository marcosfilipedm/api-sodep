package com.sodep.apisodep.service;

import com.sodep.apisodep.exception.InvalidEmailException;
import com.sodep.apisodep.model.dto.UserEntityDTO;
import com.sodep.apisodep.model.dto.UserViewDTO;

public interface UserService {

	UserViewDTO save(UserEntityDTO user) throws InvalidEmailException;

	Boolean checkEmail(String email);
	
	UserViewDTO getUserByEmail(String email);
	
	UserViewDTO getUserById(Integer id);
	
	UserEntityDTO getUser(Integer id);
}
