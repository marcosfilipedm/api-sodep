package com.sodep.apisodep.service.impl;

import java.util.Date;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sodep.apisodep.exception.InvalidEmailException;
import com.sodep.apisodep.mapper.UserMapper;
import com.sodep.apisodep.model.User;
import com.sodep.apisodep.model.dto.UserEntityDTO;
import com.sodep.apisodep.model.dto.UserViewDTO;
import com.sodep.apisodep.repository.UserRepository;
import com.sodep.apisodep.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private UserRepository userRepository;
	private UserMapper userMapper;

	public UserServiceImpl(BCryptPasswordEncoder bCryptPasswordEncoder, UserRepository userRepository, UserMapper userMapper) {
		super();
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userRepository = userRepository;
		this.userMapper = userMapper;
	}

	@Override
	public UserViewDTO save(UserEntityDTO user) throws InvalidEmailException {
		
		if(!checkEmail(user.getEmail()) || user.getId() != null) {
			User userRegister = userMapper.toEntity(user);
			userRegister.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
			userRegister.setInclusionDate(new Date());
			userRegister.setStatus(true);
			return userMapper.toViewDto(userRepository.saveAndFlush(userRegister));
		}else {
			throw new InvalidEmailException("E-mail already registered!");
		}
		
	}

	@Override
	public Boolean checkEmail(String email) {
		return userRepository.getUserByEmail(email) != null ? true : false;
	}

	@Override
	public UserViewDTO getUserByEmail(String email) {
		if(checkEmail(email)) {
			return userRepository.getUserByEmail(email);
		}else {
			return null;
		}
	}

	@Override
	public UserViewDTO getUserById(Integer id) {
		// TODO Auto-generated method stub
		return userMapper.toViewDto(userRepository.findById(id).get());
	}
	
	@Override
	public UserEntityDTO getUser(Integer id) {
		UserEntityDTO user = userMapper.toDto(userRepository.findById(id).get());
		user.setPassword(null);
		return user;
	}
}
