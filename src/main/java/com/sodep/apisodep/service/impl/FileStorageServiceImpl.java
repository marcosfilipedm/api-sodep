package com.sodep.apisodep.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.sodep.apisodep.service.FileStorageService;

@Service
public class FileStorageServiceImpl implements FileStorageService {

	private AmazonS3 s3Client;
	
	@Value("${cloud.aws.credentials.access-key}")
	private String accesKey;
	
	@Value("${cloud.aws.credentials.secret-key}")
	private String secretKey;
	
	@Value("${application.bucket.name}")
	private String bucketName;
	
	@Value("${application.bucket.url}")
	private String bucketUrl;
	
	@SuppressWarnings("deprecation")
	@PostConstruct
	private void initializeAmazon() {
		AWSCredentials credentials = new BasicAWSCredentials(this.accesKey, this.secretKey);
		this.s3Client = new AmazonS3Client(credentials);
	}
	
	@Override
	public String uploadFile(MultipartFile mFile, String fileName) {
		String fileUrl = "";
		try {
			File file = convertMultipartToFile(mFile);
			fileUrl = this.bucketUrl + "/" + fileName;
			
			s3Client.putObject(new PutObjectRequest(this.bucketName, fileName, file).withCannedAcl(CannedAccessControlList.PublicRead));
		}catch(Exception e) {
			e.printStackTrace();
		}
		return fileUrl;
	}

	private File convertMultipartToFile(MultipartFile mFile) throws IOException {
		// TODO Auto-generated method stub
		File file = new File(mFile.getOriginalFilename());
		FileOutputStream out = new  FileOutputStream(file);
		out.write(mFile.getBytes());
		out.close();
		return file;
	}

}
