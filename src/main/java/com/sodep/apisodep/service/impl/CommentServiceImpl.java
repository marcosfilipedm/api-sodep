package com.sodep.apisodep.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.sodep.apisodep.mapper.CommentMapper;
import com.sodep.apisodep.model.dto.CommentDTO;
import com.sodep.apisodep.model.dto.CommentEntityDTO;
import com.sodep.apisodep.repository.CommentRepository;
import com.sodep.apisodep.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService {

	private CommentRepository commentRepository;
	private CommentMapper commentMapper;
	
	public CommentServiceImpl(CommentRepository commentRepository, CommentMapper commentMapper) {
		super();
		this.commentRepository = commentRepository;
		this.commentMapper = commentMapper;
	}
	
	@Override
	public boolean saveComment(CommentEntityDTO commentDTO) {
		return commentRepository.saveAndFlush(commentMapper.toEntity(commentDTO)).getId() != null;
	}
	
	@Override
	public List<CommentDTO> getCommentByPost(Integer postId){
		return commentRepository.getCommentByPost(postId).stream().map(x -> new CommentDTO(x.getUserName(), x.getComment())).collect(Collectors.toList());
	}
}
