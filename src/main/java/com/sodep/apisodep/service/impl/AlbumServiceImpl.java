package com.sodep.apisodep.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.sodep.apisodep.mapper.AlbumMapper;
import com.sodep.apisodep.mapper.UserMapper;
import com.sodep.apisodep.model.Album;
import com.sodep.apisodep.model.dto.AlbumEntityDTO;
import com.sodep.apisodep.model.dto.UserEntityDTO;
import com.sodep.apisodep.repository.AlbumRepository;
import com.sodep.apisodep.service.AlbumService;
import com.sodep.apisodep.service.PostService;
import com.sodep.apisodep.service.UserService;

@Service
public class AlbumServiceImpl implements AlbumService{

	private AlbumRepository albumRepository;
	private AlbumMapper albumMapper;
	private UserService userService;
	private UserMapper userMapper;
	private PostService postService;

	public AlbumServiceImpl(AlbumRepository albumRepository, AlbumMapper albumMapper, UserService userService,
			UserMapper userMapper, PostService postService) {
		super();
		this.albumRepository = albumRepository;
		this.albumMapper = albumMapper;
		this.userService = userService;
		this.userMapper = userMapper;
		this.postService = postService;
	}

	@Override
	public boolean save(String userName, String albumName) {
		AlbumEntityDTO album = new AlbumEntityDTO(albumName, new Date());
		
		Album albumEntity = albumMapper.toEntity(album);
		albumEntity.setUser(userMapper.toEntity(userService.getUserByEmail(userName)));
		
		return albumRepository.saveAndFlush(albumEntity).getId() != null;
	}
	
	@Override	
	public List<AlbumEntityDTO> getAlbum() {
		return albumRepository.findAll().stream().map(x -> new AlbumEntityDTO(x.getId(), x.getName(), x.getInclusionDate(), getUser(x.getUser().getId()))).collect(Collectors.toList());
	}

	private UserEntityDTO getUser(Integer id) {
		return userService.getUser(id);
	}

	@Override
	public List<String> getPhotos(Integer idAlbum) {
		return postService.albumPhotos(idAlbum);
	}
}
