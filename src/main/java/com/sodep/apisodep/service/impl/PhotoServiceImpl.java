package com.sodep.apisodep.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sodep.apisodep.model.Photo;
import com.sodep.apisodep.model.dto.PhotoEntityDTO;
import com.sodep.apisodep.repository.AlbumRepository;
import com.sodep.apisodep.repository.PhotoRepository;
import com.sodep.apisodep.service.FileStorageService;
import com.sodep.apisodep.service.PhotoService;

@Service
public class PhotoServiceImpl implements PhotoService {
	
	private AlbumRepository albumRepository;
	private FileStorageService fileStorageService;
	private PhotoRepository photoRepository;

	public PhotoServiceImpl(AlbumRepository albumRepository, FileStorageService fileStorageService,
			PhotoRepository photoRepository) {
		super();
		this.albumRepository = albumRepository;
		this.fileStorageService = fileStorageService;
		this.photoRepository = photoRepository;
	}

	@Override
	public void savePhoto(MultipartFile imageFile, Integer idAlbum) {
		
		Photo photo = new Photo();
		photo.setAlbum(albumRepository.findById(idAlbum).get());
		photo.setInclusionDate(new Date());
		photo.setImageUrl(fileStorageService.uploadFile(imageFile, imageFile.getOriginalFilename()));
		photoRepository.saveAndFlush(photo);
	}
	
	@Override
	public List<PhotoEntityDTO> getPhotos(Integer albumId){
		return photoRepository.getPhotos(albumId).stream().map(x -> new PhotoEntityDTO(x.getId(), x.getInclusionDate(), x.getImageUrl())).collect(Collectors.toList());
	}

}
