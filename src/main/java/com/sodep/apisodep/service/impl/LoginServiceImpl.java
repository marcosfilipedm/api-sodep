package com.sodep.apisodep.service.impl;

import java.util.ArrayList;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sodep.apisodep.exception.InvalidEmailException;
import com.sodep.apisodep.exception.InvalidPasswordException;
import com.sodep.apisodep.jwt.JWTUsernamePasswordAuthenticationToken;
import com.sodep.apisodep.jwt.TokenProvider;
import com.sodep.apisodep.model.dto.UserAuthDTO;
import com.sodep.apisodep.model.dto.UserViewDTO;
import com.sodep.apisodep.repository.UserRepository;
import com.sodep.apisodep.service.LoginService;
import com.sodep.apisodep.service.UserService;

@Service
public class LoginServiceImpl implements LoginService{

	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private UserService userService;
	private UserRepository userRepository;
	private TokenProvider tokenProvider;

	public LoginServiceImpl(BCryptPasswordEncoder bCryptPasswordEncoder, UserService userService,
			UserRepository userRepository, TokenProvider tokenProvider) {
		super();
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userService = userService;
		this.userRepository = userRepository;
		this.tokenProvider = tokenProvider;
	}

	@Override
	public String login(String username, String password, String system) throws InvalidPasswordException, InvalidEmailException {
		
		if(userService.checkEmail(username)) {
			UserViewDTO userDTO = userRepository.getUserByEmail(username);
			
			if(checkPassword(password, userRepository.getById(userDTO.getId()).getPassword())) {
				
				UserAuthDTO userAuth = new UserAuthDTO(userDTO.getId(), username, password, system, true);
				JWTUsernamePasswordAuthenticationToken userAuthenticationToken = new JWTUsernamePasswordAuthenticationToken(username,password, new ArrayList<>());
				userAuthenticationToken.setDetails(userDTO.getId());
				SecurityContextHolder.getContext().setAuthentication(userAuthenticationToken);
				
				//TODO: Perfil fixo temporário
				return tokenProvider.createToken(userAuthenticationToken, userAuth, system, 1);
			}else {
				throw new InvalidPasswordException("Invalid Password");
			}
		}else {
			throw new InvalidEmailException("E-mail not registered!");
		}
	}
	
	public Boolean checkPassword(String credentialsPassword, String userPassword) {
		return bCryptPasswordEncoder.matches(credentialsPassword, userPassword);
	}
	
}
