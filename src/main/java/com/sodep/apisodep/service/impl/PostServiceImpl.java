package com.sodep.apisodep.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.sodep.apisodep.mapper.PostMapper;
import com.sodep.apisodep.model.Post;
import com.sodep.apisodep.model.dto.PostEntityDTO;
import com.sodep.apisodep.model.dto.PostViewDTO;
import com.sodep.apisodep.repository.AlbumRepository;
import com.sodep.apisodep.repository.PostRepository;
import com.sodep.apisodep.service.CommentService;
import com.sodep.apisodep.service.FileStorageService;
import com.sodep.apisodep.service.PostService;
import com.sodep.apisodep.service.UserService;

@Service
public class PostServiceImpl implements PostService {
	
	private PostRepository postRepository;
	private UserService userService;
	private PostMapper postMapper;
	private FileStorageService fileStorageService;
	private CommentService commentService;
	private AlbumRepository albumRepository;

	public PostServiceImpl(PostRepository postRepository, UserService userService, PostMapper postMapper,
			FileStorageService fileStorageService, CommentService commentService, AlbumRepository albumRepository) {
		super();
		this.postRepository = postRepository;
		this.userService = userService;
		this.postMapper = postMapper;
		this.fileStorageService = fileStorageService;
		this.commentService = commentService;
		this.albumRepository = albumRepository;
	}

	@Override
	public PostEntityDTO savePost(String legend, String username, MultipartFile imageFile) {
		
		PostEntityDTO post = new PostEntityDTO();
		
		try {
			post.setLegend(legend);
			post.setStatus(true);
			post.setInclusionDate(new Date());
			post.setUser(userService.getUserByEmail(username));
			post.setImageUrl(fileStorageService.uploadFile(imageFile, imageFile.getOriginalFilename()));
			post.setId(postRepository.saveAndFlush(postMapper.toEntity(post)).getId());
		}catch(Exception e) {
			e.printStackTrace();
		}	
		return post;
	}

	@Override
	public PostEntityDTO getPost(Integer idPost) {
		return postMapper.toDto(postRepository.findById(idPost).get());
	}

	@Override
	public List<PostViewDTO> getPosts() {
		return postRepository.readAll().stream().map(x -> new PostViewDTO(x.getId(), x.getLegend(), x.getInclusionDate(), x.getImageUrl(), userService.getUserById(x.getUserId()), commentService.getCommentByPost(x.getId()))).collect(Collectors.toList()); 
	}

	@Override
	public boolean alterPostStatus(Integer idPost) {

		// TODO: Pensar sobre a data do post.
		Post post = postRepository.findById(idPost).get();
		if (post.getStatus())
			post.setStatus(false);
		else
			post.setStatus(true);

		return postRepository.saveAndFlush(post) != null;
	}

	@Override
	public List<PostEntityDTO> getMyPosts(String name) {
		Integer userId = userService.getUserByEmail(name).getId();
		return postRepository.getPostsByUserId(userId).stream().map(x -> new PostEntityDTO(x.getId(), userService.getUserById(x.getUserId()), x.getLegend(), x.getStatus(), x.getInclusionDate(), x.getImageUrl())).collect(Collectors.toList());
	}

	@Override
	public void addAlbum(Integer idPost, Integer idAlbum) {
		Post post = postRepository.findById(idPost).get();
		post.setAlbum(albumRepository.findById(idAlbum).get());
		postRepository.saveAndFlush(post);
	}
	
	@Override
	public List<String> albumPhotos(Integer album){
		return postRepository.getAlbumPhotos(album);
	}
}
