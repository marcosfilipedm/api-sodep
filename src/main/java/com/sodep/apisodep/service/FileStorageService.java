package com.sodep.apisodep.service;

import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {

	public String uploadFile(MultipartFile file, String fileName);

}
