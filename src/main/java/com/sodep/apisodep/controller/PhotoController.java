package com.sodep.apisodep.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.sodep.apisodep.model.dto.PhotoEntityDTO;
import com.sodep.apisodep.service.PhotoService;

@Controller
@RequestMapping("/photo")
public class PhotoController {

	private PhotoService photoService;

	public PhotoController(PhotoService photoService) {
		super();
		this.photoService = photoService;
	}
	
	@PostMapping
	public ResponseEntity<?> savePhoto(@RequestParam("imageFile") MultipartFile imageFile, @RequestParam("album") Integer idAlbum){
		try {
			photoService.savePhoto(imageFile, idAlbum);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/{idAlbum}")
	public ResponseEntity<List<PhotoEntityDTO>> getPhotos(@PathVariable("idAlbum") Integer idAlbum){
		try {
			return ResponseEntity.ok(photoService.getPhotos(idAlbum));
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
