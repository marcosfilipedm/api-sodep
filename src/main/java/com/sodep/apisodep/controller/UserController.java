package com.sodep.apisodep.controller;

import java.security.Principal;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sodep.apisodep.exception.InvalidEmailException;
import com.sodep.apisodep.exception.InvalidPasswordException;
import com.sodep.apisodep.jwt.JWTConfigurer;
import com.sodep.apisodep.jwt.JwtToken;
import com.sodep.apisodep.model.dto.UserEntityDTO;
import com.sodep.apisodep.model.dto.UserViewDTO;
import com.sodep.apisodep.service.LoginService;
import com.sodep.apisodep.service.UserService;
import com.sodep.apisodep.utils.SecurityConstants;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("/user")
public class UserController {
	
	public static final String USERNAME_HEADER = "username";
	public static final String KEY_HEADER = "password";
	public static final String ORIGIN = "origin";

	private UserService userService;
	private LoginService loginService;
	
	public UserController(UserService userService, LoginService loginService) {
		super();
		this.userService = userService;
		this.loginService = loginService;
	}

	@PostMapping(path = "/public/save", consumes = "application/json")
	public ResponseEntity<UserViewDTO> saveUser(@RequestBody UserEntityDTO user) {
		
		try {
			return ResponseEntity.ok(userService.save(user));
		} catch (InvalidEmailException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/public/login")
	public ResponseEntity<JwtToken> login(@RequestHeader(USERNAME_HEADER) String username, @RequestHeader(KEY_HEADER) String password, @RequestHeader(ORIGIN) String system) {
		try {
			
			String jwt = loginService.login(username, password, system);
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.add(JWTConfigurer.AUTHORIZATION_HEADER, jwt);
			return new ResponseEntity<>(new JwtToken(SecurityConstants.JWT_PREFIX + jwt), httpHeaders, HttpStatus.OK);
		} catch (InvalidPasswordException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (InvalidEmailException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
	}

	@GetMapping()
	public ResponseEntity<UserViewDTO> getUser(@ApiIgnore Principal principal) {
		return ResponseEntity.ok(userService.getUserByEmail(principal.getName()));
	}
}
