package com.sodep.apisodep.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.sodep.apisodep.model.dto.PostEntityDTO;
import com.sodep.apisodep.model.dto.PostViewDTO;
import com.sodep.apisodep.service.PostService;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("/post")
public class PostController {

	private PostService postService;

	public PostController(PostService postService) {
		super();
		this.postService = postService;
	}
	
	@PostMapping("/save")
	public ResponseEntity<PostEntityDTO> savePost(@RequestParam("imageFile") MultipartFile imageFile, @RequestParam("legend") String legend, @ApiIgnore Principal principal){
		
		System.out.println();
		return ResponseEntity.ok(postService.savePost(legend, principal.getName(), imageFile));
	}
	
	@GetMapping()
	public ResponseEntity<List<PostViewDTO>> getPosts(){
		return ResponseEntity.ok(postService.getPosts());
	}
	
	@GetMapping("/{postId}")
	public ResponseEntity<PostEntityDTO> getPost(@PathVariable Integer postId){
		return ResponseEntity.ok(postService.getPost(postId));
	}
	
	@PutMapping("/alter-status")
	public ResponseEntity<Boolean> alterPostStatus(@RequestBody Integer postId){
		return ResponseEntity.ok(postService.alterPostStatus(postId));
	}
	
	@GetMapping("/my-posts")
	public ResponseEntity<List<PostEntityDTO>> getMyPosts(@ApiIgnore Principal principal){
		return ResponseEntity.ok(postService.getMyPosts(principal.getName()));
	}
	
	@PutMapping("/add-album")
	public ResponseEntity<?> alterPostStatus(@RequestParam("post") Integer post, @RequestParam("album") Integer album) {
		try {
			postService.addAlbum(post, album);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
