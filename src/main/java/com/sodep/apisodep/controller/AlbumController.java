package com.sodep.apisodep.controller;

import java.security.Principal;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sodep.apisodep.service.AlbumService;

import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("/album")
public class AlbumController {
	
	private AlbumService albumService;

	public AlbumController(AlbumService albumService) {
		super();
		this.albumService = albumService;
	}

	@PostMapping()
	public ResponseEntity<?> save(@ApiIgnore Principal principal, @RequestBody String albumName){
		return ResponseEntity.ok(albumService.save(principal.getName(), albumName));
	}
	
	@GetMapping()
	public ResponseEntity<?> getAlbuns(){
		return ResponseEntity.ok(albumService.getAlbum());
	}
	
	@GetMapping("/{idAlbum}/photos")
	public ResponseEntity<?> getPhotos(@PathVariable("idAlbum") Integer idAlbum){
		return ResponseEntity.ok(albumService.getPhotos(idAlbum));
	}
}
