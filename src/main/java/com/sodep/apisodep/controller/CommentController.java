package com.sodep.apisodep.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sodep.apisodep.model.dto.CommentEntityDTO;
import com.sodep.apisodep.service.CommentService;

@Controller
@RequestMapping("/comment")
public class CommentController {

	private CommentService commentService;

	public CommentController(CommentService commentService) {
		super();
		this.commentService = commentService;
	}
	
	
	@PostMapping(path = "/save", consumes = "application/json")
	public ResponseEntity<?> saveComment(@RequestBody CommentEntityDTO comment){
		return ResponseEntity.ok(commentService.saveComment(comment));
	}
}
