package com.sodep.apisodep.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sodep.apisodep.model.User;
import com.sodep.apisodep.model.dto.UserViewDTO;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

	@Query("select new com.sodep.apisodep.model.dto.UserViewDTO(u.id, u.name, u.cpf, u.email, u.status, u.inclusionDate) from User u where email like :email")
	public UserViewDTO getUserByEmail(@Param("email") String email);
	
}
