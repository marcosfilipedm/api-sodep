package com.sodep.apisodep.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sodep.apisodep.model.Post;
import com.sodep.apisodep.model.dto.PostEntityDTO;
import com.sodep.apisodep.model.dto.PostViewDTO;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer>{

	@Query("select new com.sodep.apisodep.model.dto.PostViewDTO(p.id, p.legend, p.inclusionDate, p.imageUrl, p.user.id) from Post p where p.status = 1 order by p.id desc")
	List<PostViewDTO> readAll();
	
	@Query("select new com.sodep.apisodep.model.dto.PostEntityDTO(p.id, p.user.id, p.legend, p.status, p.inclusionDate, p.imageUrl) from Post p where p.user.id =:id order by p.id desc")
	List<PostEntityDTO> getPostsByUserId(@Param("id") Integer id);
	
	@Query("select p.imageUrl from Post p where p.album.id =:id")
	List<String> getAlbumPhotos(@Param("id") Integer id);

}
