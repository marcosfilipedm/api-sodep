package com.sodep.apisodep.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sodep.apisodep.model.Album;
import com.sodep.apisodep.model.dto.AlbumEntityDTO;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Integer>{

	@Query("select new com.sodep.apisodep.model.dto.AlbumEntityDTO(a.id, a.name, a.inclusionDate) from Album a order by a.inclusionDate desc")
	public List<AlbumEntityDTO> getAlbum();
}
