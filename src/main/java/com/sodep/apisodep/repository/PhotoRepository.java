package com.sodep.apisodep.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sodep.apisodep.model.Photo;
import com.sodep.apisodep.model.dto.PhotoEntityDTO;

@Repository
public interface PhotoRepository extends JpaRepository<Photo, Integer> {

	@Query("select new com.sodep.apisodep.model.dto.PhotoEntityDTO(p.id, p.inclusionDate, p.imageUrl) from Photo p where p.album.id =:idAlbum ")
	public List<PhotoEntityDTO> getPhotos(@Param("idAlbum") Integer idAlbum);
}
