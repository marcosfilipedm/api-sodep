package com.sodep.apisodep.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sodep.apisodep.model.Comment;
import com.sodep.apisodep.model.dto.CommentDTO;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{

	@Query("select new com.sodep.apisodep.model.dto.CommentDTO(c.user.name, c.comment) from Comment c where c.post.id = :postId order by c.id")
	public List<CommentDTO> getCommentByPost(@Param("postId") Integer postId);
}
