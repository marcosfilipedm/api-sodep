package com.sodep.apisodep.utils;

public class SecurityConstants {
	
	public static final String SECRET = "6402cf1325132fdacafed908cfdbeaaa7333";
	public static final long EXPIRATION_TIME = 3600000; // 5min
	public static final String JWT_PREFIX = "Bearer ";
	public static final String CLAIM_LOGIN = "login";
	public static final String CLAIM_DETAIL = "details";
	public static final String CLAIM_ID = "id";
	public static final String HEADER_STRING = "Authorization";
}
