package com.sodep.apisodep.jwt;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JWTConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity>{

	/**
     * Nome da autorização
     */
    public static final String AUTHORIZATION_HEADER = "Authorization";

    /**
     * Declaração de TokenProvider
     */
    private TokenProvider tokenProvider;

    /**
     * 
     * Construtor da classe
     * 
     * @param tokenProvider
     */
    public JWTConfigurer(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    /**
     * Método que configura o
     * JWTFilter
     * 
     * para ser usado no início das
     * 
     * requisições
     * 
     * @param http
     */
    @Override
    public void configure(HttpSecurity http) {
        JWTFilter customFilter = new JWTFilter(tokenProvider);
        http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
