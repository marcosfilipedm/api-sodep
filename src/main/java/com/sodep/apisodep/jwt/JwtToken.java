package com.sodep.apisodep.jwt;

public class JwtToken {

	private String idToken;

	public JwtToken(String idToken) {
		super();
		this.idToken = idToken;
	}

	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	
}
