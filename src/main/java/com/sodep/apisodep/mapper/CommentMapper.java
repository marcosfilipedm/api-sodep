package com.sodep.apisodep.mapper;

import org.mapstruct.Mapper;

import com.sodep.apisodep.model.Comment;
import com.sodep.apisodep.model.dto.CommentEntityDTO;

@Mapper(componentModel = "spring")
public interface CommentMapper {

	Comment toEntity(CommentEntityDTO dto);
	
	CommentEntityDTO toDto(Comment entity);
}
