package com.sodep.apisodep.mapper;

import org.mapstruct.Mapper;

import com.sodep.apisodep.model.Post;
import com.sodep.apisodep.model.dto.PostEntityDTO;

@Mapper(componentModel = "spring")
public interface PostMapper {

	Post toEntity(PostEntityDTO dto);
	
	PostEntityDTO toDto(Post entity);
}
