package com.sodep.apisodep.mapper;

import org.mapstruct.Mapper;

import com.sodep.apisodep.model.User;
import com.sodep.apisodep.model.dto.UserEntityDTO;
import com.sodep.apisodep.model.dto.UserViewDTO;


@Mapper(componentModel = "spring")
public interface UserMapper {

	User toEntity(UserEntityDTO dto);
	
	User toEntity(UserViewDTO dto);
	
	UserEntityDTO toDto(User entity);
	
	UserViewDTO toViewDto(User entity);
	
}
