package com.sodep.apisodep.mapper;

import org.mapstruct.Mapper;

import com.sodep.apisodep.model.Album;
import com.sodep.apisodep.model.dto.AlbumEntityDTO;

@Mapper(componentModel = "spring")
public interface AlbumMapper {

	Album toEntity(AlbumEntityDTO dto);
	
	AlbumEntityDTO toDto(Album entity);
}
