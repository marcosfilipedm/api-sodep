package com.sodep.apisodep.mapper;

import org.mapstruct.Mapper;

import com.sodep.apisodep.model.Photo;
import com.sodep.apisodep.model.dto.PhotoEntityDTO;

@Mapper(componentModel = "spring")
public interface PhotoMapper {

	Photo toEntity(PhotoEntityDTO dto);
	
	PhotoEntityDTO toDto(Photo entity);
}
