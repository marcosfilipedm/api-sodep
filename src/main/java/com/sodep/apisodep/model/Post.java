package com.sodep.apisodep.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "POSTS")
public class Post implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -562342289758707122L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID", referencedColumnName = "ID")
	private User user;
	
	@Column(name = "LEGEND")
	private String legend;
	
	@Column(name = "STATUS")
	private Boolean status;

	@Column(name = "INCLUSION_DATE")
	private Date inclusionDate;
	
	@Column(name = "EXTENTION")
	private String imageExtention;
	
	@Column(name = "IMAGE_URL")
	private String imageUrl;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "post")
	List<Comment> comments;
	
	@ManyToOne
	@JoinColumn(name = "ALBUM_ID", referencedColumnName = "ID")
	private Album album;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getLegend() {
		return legend;
	}

	public void setLegend(String legend) {
		this.legend = legend;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getInclusionDate() {
		return inclusionDate;
	}

	public void setInclusionDate(Date inclusionDate) {
		this.inclusionDate = inclusionDate;
	}

	public String getImageExtention() {
		return imageExtention;
	}

	public void setImageExtention(String imageExtention) {
		this.imageExtention = imageExtention;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}
}
