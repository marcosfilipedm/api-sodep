package com.sodep.apisodep.model.dto;

public class UserAuthDTO {
	
	private Integer id;

	private String login;
	
	private String senha;

	private String sistema;

	private boolean autenticado;

	public UserAuthDTO() {
		super();
	}

	public UserAuthDTO(boolean autenticado) {
		super();
		this.autenticado = autenticado;
	}

	public UserAuthDTO(Integer id, String login, String senha, String sistema, boolean autenticado) {
		super();
		this.id = id;
		this.login = login;
		this.senha = senha;
		this.sistema = sistema;
		this.autenticado = autenticado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public boolean isAutenticado() {
		return autenticado;
	}

	public void setAutenticado(boolean autenticado) {
		this.autenticado = autenticado;
	}
	
}
