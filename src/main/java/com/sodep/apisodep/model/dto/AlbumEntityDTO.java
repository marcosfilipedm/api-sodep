package com.sodep.apisodep.model.dto;

import java.util.Date;

public class AlbumEntityDTO {

	private Integer id;
	
	private String name;
	
	private Date inclusionDate;
	
	private UserEntityDTO user;

	public AlbumEntityDTO() {
		super();
	}

	public AlbumEntityDTO(Integer id, String name, Date inclusionDate, UserEntityDTO user) {
		super();
		this.id = id;
		this.name = name;
		this.inclusionDate = inclusionDate;
		this.user = user;
	}
	

	public AlbumEntityDTO(String name, Date inclusionDate) {
		super();
		this.name = name;
		this.inclusionDate = inclusionDate;
	}

	public AlbumEntityDTO(Integer id, String name, Date inclusionDate) {
		super();
		this.id = id;
		this.name = name;
		this.inclusionDate = inclusionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getInclusionDate() {
		return inclusionDate;
	}

	public void setInclusionDate(Date inclusionDate) {
		this.inclusionDate = inclusionDate;
	}

	public UserEntityDTO getUser() {
		return user;
	}

	public void setUser(UserEntityDTO user) {
		this.user = user;
	}
	
}
