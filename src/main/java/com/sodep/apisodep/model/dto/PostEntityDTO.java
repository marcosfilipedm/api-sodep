package com.sodep.apisodep.model.dto;

import java.util.Date;

public class PostEntityDTO {

	private Integer id;
	
	private Integer userId;
	
	private UserViewDTO user;
	
	private String legend;
	
	private Boolean status;

	private Date inclusionDate;
	
	private String imageUrl;

	public PostEntityDTO() {
		super();
	}

	public PostEntityDTO(Integer id, Integer userId, String legend, Boolean status, Date inclusionDate, String imageUrl) {
		super();
		this.id = id;
		this.userId = userId;
		this.legend = legend;
		this.status = status;
		this.inclusionDate = inclusionDate;
		this.imageUrl = imageUrl;
	}

	public PostEntityDTO(Integer id, UserViewDTO user, String legend, Boolean status, Date inclusionDate, String imageUrl) {
		super();
		this.id = id;
		this.user = user;
		this.legend = legend;
		this.status = status;
		this.inclusionDate = inclusionDate;
		this.imageUrl = imageUrl;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public UserViewDTO getUser() {
		return user;
	}

	public void setUser(UserViewDTO user) {
		this.user = user;
	}

	public String getLegend() {
		return legend;
	}

	public void setLegend(String legend) {
		this.legend = legend;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getInclusionDate() {
		return inclusionDate;
	}

	public void setInclusionDate(Date inclusionDate) {
		this.inclusionDate = inclusionDate;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

}
