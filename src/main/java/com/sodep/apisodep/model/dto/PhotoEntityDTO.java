package com.sodep.apisodep.model.dto;

import java.util.Date;

import com.sodep.apisodep.model.Album;

public class PhotoEntityDTO {

	private Integer id;
	
	private Date inclusionDate;
	
	private Album album;

	private String imageUrl;
	
	public PhotoEntityDTO() {
		super();
	}

	public PhotoEntityDTO(Integer id, Date inclusionDate, Album album) {
		super();
		this.id = id;
		this.inclusionDate = inclusionDate;
		this.album = album;
	}

	public PhotoEntityDTO(Integer id, Date inclusionDate, String imageUrl) {
		super();
		this.id = id;
		this.inclusionDate = inclusionDate;
		this.imageUrl = imageUrl;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getInclusionDate() {
		return inclusionDate;
	}

	public void setInclusionDate(Date inclusionDate) {
		this.inclusionDate = inclusionDate;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
