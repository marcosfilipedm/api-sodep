package com.sodep.apisodep.model.dto;

import java.util.Date;

public class UserViewDTO {

	private Integer id;
	
	private String name;
	
	private String cpf;
	
	private String email;
		
	private Boolean status;
	
	private Date inclusionDate;
	

	public UserViewDTO() {
		super();
	}

	public UserViewDTO(Integer id, String name, String cpf, String email, Boolean status, Date inclusionDate) {
		super();
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.email = email;
		this.status = status;
		this.inclusionDate = inclusionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getInclusionDate() {
		return inclusionDate;
	}

	public void setInclusionDate(Date inclusionDate) {
		this.inclusionDate = inclusionDate;
	}
}
