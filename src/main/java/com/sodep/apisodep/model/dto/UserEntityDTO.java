package com.sodep.apisodep.model.dto;

import java.util.Date;

public class UserEntityDTO {

	private Integer id;
	
	private String name;
	
	private String cpf;
	
	private String email;
	
	private String password;
	
	private Boolean status;
	
	private Date inclusionDate;
	
	private Integer idPost;

	public UserEntityDTO() {
		super();
	}

	public UserEntityDTO(String name, String cpf, String email, String password, Boolean status) {
		super();
		this.name = name;
		this.cpf = cpf;
		this.email = email;
		this.password = password;
		this.status = status;
	}

	public UserEntityDTO(Integer id, String name, String cpf, String email, String password, Boolean status) {
		super();
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.email = email;
		this.password = password;
		this.status = status;
	}

	
	public UserEntityDTO(Integer id, String name, String cpf, String password, Boolean status, Date inclusionDate) {
		super();
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.password = password;
		this.status = status;
		this.inclusionDate = inclusionDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getInclusionDate() {
		return inclusionDate;
	}

	public void setInclusionDate(Date inclusionDate) {
		this.inclusionDate = inclusionDate;
	}

	public Integer getIdPost() {
		return idPost;
	}

	public void setIdPost(Integer idPost) {
		this.idPost = idPost;
	}
	
}
