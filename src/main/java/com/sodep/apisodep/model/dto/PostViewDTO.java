package com.sodep.apisodep.model.dto;

import java.util.Date;
import java.util.List;

public class PostViewDTO {
	
	private Integer id;
	
	private String legend;

	private Date inclusionDate;
	
	private String imageUrl;
	
	private UserViewDTO user;
	
	private List<CommentDTO> comments;
	
	private Integer userId;
	
	public PostViewDTO() {
		super();
	}

	public PostViewDTO(Integer id, String legend, Date inclusionDate, String imageUrl, Integer userId) {
		super();
		this.id = id;
		this.legend = legend;
		this.inclusionDate = inclusionDate;
		this.imageUrl = imageUrl;
		this.userId = userId;
	}

	public PostViewDTO(Integer id, String legend, Date inclusionDate, String imageUrl, UserViewDTO user, List<CommentDTO> comments) {
		super();
		this.id = id;
		this.legend = legend;
		this.inclusionDate = inclusionDate;
		this.imageUrl = imageUrl;
		this.user = user;
		this.comments = comments;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLegend() {
		return legend;
	}

	public void setLegend(String legend) {
		this.legend = legend;
	}

	public Date getInclusionDate() {
		return inclusionDate;
	}

	public void setInclusionDate(Date inclusionDate) {
		this.inclusionDate = inclusionDate;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public UserViewDTO getUser() {
		return user;
	}

	public void setUser(UserViewDTO user) {
		this.user = user;
	}

	public List<CommentDTO> getComments() {
		return comments;
	}

	public void setComments(List<CommentDTO> comments) {
		this.comments = comments;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}
