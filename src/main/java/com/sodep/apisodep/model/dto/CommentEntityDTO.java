package com.sodep.apisodep.model.dto;

import com.sodep.apisodep.model.Post;
import com.sodep.apisodep.model.User;

public class CommentEntityDTO {

	private Integer id;
	
	private Post post;
	
	private User user;
	
	private String comment;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
