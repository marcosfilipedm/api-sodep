package com.sodep.apisodep.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sodep.apisodep.exception.InvalidEmailException;
import com.sodep.apisodep.model.dto.UserEntityDTO;
import com.sodep.apisodep.model.dto.UserViewDTO;
import com.sodep.apisodep.service.UserService;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestMethodOrder(OrderAnnotation.class)
public class UserServiceTests {

	@Autowired
	private UserService userService;
	
	@Test
	@Order(1)
	public void saveUser() {
		
		try {
			UserEntityDTO userEntity = new UserEntityDTO(null, "admin", "213.649.946-74", "admin@sodep.com", "admin", true);
			UserViewDTO userView;
			userView = userService.save(userEntity);
			
			Integer expected = 1;
			
			assertEquals(expected, userView.getId());
		} catch (InvalidEmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	@Order(2)
	public void checkEmail() {
		String email = "admin@sodep.com";
		assertTrue(userService.checkEmail(email));
	}
	
	@Test
	@Order(3)
	public void getUserByEmail() {
		Integer expected = 1;
		Integer userId = userService.getUserByEmail("admin@sodep.com").getId();
		assertEquals(expected, userId);
	}
	
	@Test
	@Order(4)
	public void getUserById() {
		Integer expected = 1;
		Integer userId = userService.getUserById(expected).getId();
		assertEquals(expected, userId);
	}
	
	@Test
	@Order(5)
	public void getUser() {
		Integer expected = 1;
		Integer userId = userService.getUser(expected).getId();
		assertEquals(expected, userId);
	}
}
