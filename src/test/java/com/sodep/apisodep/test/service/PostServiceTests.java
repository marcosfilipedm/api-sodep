package com.sodep.apisodep.test.service;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sodep.apisodep.model.dto.PostEntityDTO;
import com.sodep.apisodep.service.PostService;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestMethodOrder(OrderAnnotation.class)
public class PostServiceTests {

	@Autowired
	private PostService postService;
	
	@Test
	@Order(1)
	public void savePost() {
		
		try {
			String expected = "test";
			String email = "admin@sodep.com";
			PostEntityDTO post = postService.savePost(expected, email, null);
			assertEquals(expected, post.getLegend());
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@Order(2)
	public void getPost() {
		
		try {
			Integer expected = 1;
			PostEntityDTO postDto = postService.getPost(expected);
			assertEquals(expected, postDto.getId());
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	@Order(3)
	public void getAllPosts() {
		
		try {
			
			Integer expected = 1;			
			Integer result = postService.getPosts().size();
			assertEquals(expected, result);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@Order(4)
	public void alterPostStatus() {
		
		try {
			postService.alterPostStatus(1);
			Boolean result = postService.getPost(1).getStatus();
			
			Boolean expected = true;
			
			assertEquals(expected, result);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@Order(5)
	public void getMyPosts() {
		
		try {
			String email = "admin@sodep.com";
			Integer result = postService.getMyPosts(email).size();
			Integer expected = 1;
			assertEquals(expected, result);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
